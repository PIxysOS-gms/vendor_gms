#
# Copyright (C) 2020-2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

BUILD_GMS_OVERLAYS := false

ifeq ($(BUILD_GMS_OVERLAYS),true)

PRODUCT_SOONG_NAMESPACES += \
    vendor/gms/overlay

# GMS RRO overlay packages
PRODUCT_PACKAGES += \
    GmsConfigOverlayCommon \
    GmsConfigOverlayComms \
    GmsConfigOverlayContactsProvider \
    GmsConfigOverlayGeotz \
    GmsConfigOverlayGSA \
    GmsConfigOverlaySettings \
    GmsConfigOverlaySettingsProvider \
    GmsConfigOverlayTelecom \
    GmsConfigOverlayTeleService \
    GmsConfigOverlayTurbo

# Pixel RRO overlay packages
PRODUCT_PACKAGES += \
    GoogleConfigOverlay \
    GoogleSettingsOverlay \
    GoogleSystemUIOverlay \
    PixelConfigOverlayCommon \
    PixelConfigOverlayWallpaper \
    PixelDocumentsUIGoogleOverlay \
    PixelSetupWizardOverlay \
    PixelSetupWizardOverlay2019 \
    PixelSetupWizardStringsOverlay

endif # BUILD_GMS_OVERLAYS

# Gboard
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.ime.theme_id=5 \
    ro.com.google.ime.system_lm_dir=/product/usr/share/ime/google/d3_lms

# Google SetupWizard
PRODUCT_PRODUCT_PROPERTIES += \
    ro.setupwizard.enterprise_mode=1 \
    ro.setupwizard.esim_cid_ignore=00000001 \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    setupwizard.feature.show_pai_screen_in_main_flow.carrier1839=false \
    setupwizard.feature.skip_button_use_mobile_data.carrier1839=true \
    setupwizard.theme=glif_v3_light \
    setupwizard.feature.show_pixel_tos=true \
    setupwizard.feature.show_support_link_in_deferred_setup=false \
    setupwizard.feature.enable_wifi_tracker=true \
    ro.carriersetup.vzw_consent_page=true \
    setupwizard.feature.day_night_mode_enabled=true \
    setupwizard.feature.portal_notification=true \
    setupwizard.feature.lifecycle_refactoring=true \
    setupwizard.feature.notification_refactoring=true \
     setupwizard.feature.enable_quick_start_flow=true


# StorageManager
PRODUCT_PRODUCT_PROPERTIES += \
    ro.storage_manager.show_opt_in=false

# OPA
PRODUCT_PRODUCT_PROPERTIES += \
    ro.opa.eligible_device=true

# Google Play services
PRODUCT_PRODUCT_PROPERTIES += \
    ro.atrace.core.services=com.google.android.gms,com.google.android.gms.ui,com.google.android.gms.persistent \
    ro.com.google.clientidbase=android-google \
    ro.error.receiver.system.apps=com.google.android.gms

# inherit common makefile
$(call inherit-product, vendor/gms/common/common-vendor.mk)

# Conditionally build extra packages from pixel
$(call inherit-product, vendor/gms/extras/config.mk)
